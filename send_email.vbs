' This work is licensed under a Creative Commons Attribution-ShareAlike 4.0
' International License (http://creativecommons.org/licenses/by-sa/4.0/).
'
' Based on source from SuperUser
' http://superuser.com/questions/781588/
'

sArguments = ""
For i = 0 to Wscript.Arguments.Count - 1
  if i > 0 Then
    sArguments = sArguments + " "
  End If
  sArguments = sArguments + Wscript.Arguments(i)
Next

Set iMsg = CreateObject("CDO.Message")
Set iConf = CreateObject("CDO.Configuration")
Set Flds = iConf.Fields
schema = "http://schemas.microsoft.com/cdo/configuration/"
Flds.Item(schema & "sendusing") = 2
Flds.Item(schema & "smtpserver") = "smtp.GMAIL.com"
Flds.Item(schema & "smtpserverport") = 465
Flds.Item(schema & "smtpauthenticate") = 1
Flds.Item(schema & "sendusername") = "YOU@GMAIL.COM"
Flds.Item(schema & "sendpassword") = "YOURPASSWORD"
Flds.Item(schema & "smtpusessl") = 1
Flds.Update

With iMsg
.To = "destination email"
.From = "name <YOU@GMAIL.COM"
.Subject = sArguments
.HTMLBody = message
.Sender = " "
.Organization = " "
.ReplyTo = " "
Set .Configuration = iConf
SendGMAILGmail = .Send
End With

set iMsg = nothing
set iConf = nothing
set Flds = nothing
