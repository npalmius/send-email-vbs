# Send Email using VBScript

The code in this repository sends an email using VBScript and CDO.Sys.

Based on code from SuperUser question [sen email without qoutes vbs](http://superuser.com/questions/781588/sen-email-without-qoutes-vbs) and [my answer](http://superuser.com/a/781637/293259). Credits to [user1603548](http://superuser.com/users/227188/user1603548); original source unknown.

---

[![Creative Commons Licence](https://i.creativecommons.org/l/by-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-sa/4.0/)
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).